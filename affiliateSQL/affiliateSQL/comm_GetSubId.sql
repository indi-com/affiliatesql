/****** Object:  StoredProcedure [dbo].[comm_GetSubId]    Script Date: 11/6/2020 11:24:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      BEC
-- Create Date: 10/3/2019
-- Description: Gets mapid from sub id
-- Sets @output
-- =============================================
ALTER PROCEDURE [dbo].[comm_GetSubId]
(
	@subid varchar(255) OUT,
	@custom1 varchar(255) = NULL OUT,
	@productid int = NULL OUT,
	@siteid varchar(255) = NULL OUT,
	@custom2 varchar(255) = NULL OUT,
	@custom3 varchar(255) = NULL OUT,
	@custom4 varchar(255) = NULL OUT,
	@custom5 varchar(255) = NULL OUT,
	@iscashback bit = 0 OUT
)
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @userid varchar(255) = NULL;
	SELECT @custom1=Custom1,@custom2=Custom2,@custom3=Custom3,@custom4=Custom4,@custom5=Custom5,@productid=ProductId,@siteid=SiteId,@userid=UserId,@iscashback=isCashBack FROM Management_LinkSubIds WHERE SubId=@subid;
	IF(@userid IS NOT NULL)
	BEGIN
		SET @subid=@userid;
	END
END
