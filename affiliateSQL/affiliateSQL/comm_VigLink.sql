/****** Object:  StoredProcedure [dbo].[comm_VigLink]    Script Date: 9/21/2021 10:30:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
ALTER PROCEDURE [dbo].[comm_VigLink]
(
	@record_id varchar(255),
	@commission_id varchar(255) = NULL,
	@commission_date datetime = NULL,
	@cu_id varchar(255) = NULL,
	@account_id varchar(255) = NULL,
	@click_url varchar(max) = NULL,
	@click_date datetime = NULL,
	@campaign_id varchar(255) = NULL,
	@campaign_name varchar(255) = NULL,
	@device_type varchar(255) = NULL,
	@merchant_group_id varchar(255) = NULL,
	@merchant_id varchar(255) = NULL,
	@merchant_name varchar(255) = NULL,
	@merchant_name_original varchar(255) = NULL,
	@order_value decimal(18,4) = 0,
	@purchaser_revenue decimal(18,4),
	@networkid varchar(255) = NULL,
	@merchantmapid varchar(255) = NULL,
	@processid varchar(255) = NULL
)
AS
BEGIN
DECLARE @offerid varchar(255) = @campaign_id;
DECLARE @total_commission decimal(18,4) = @purchaser_revenue;

DECLARE @custom1 varchar(255) = null,@custom2 varchar(255) = null,@custom3 varchar(255) = null,@custom4 varchar(255) = null,@custom5 varchar(255) = null,@iscashback bit = 0;
	DECLARE @mapsubid varchar(255) = @cu_id;
	DECLARE @productid int = null,@mapsiteid varchar(255) = null,@siteid varchar(255) = null;
	--Declare standard variables
  DECLARE @user_commission decimal(18,4),@remaining_commission decimal(18,4),@user_status varchar(50),@transaction_status varchar(255);

	IF LEFT(@cu_id, 4) = 'cus_' 
	BEGIN
		EXEC comm_CustomCommission @record_id=@record_id,@transaction_id=@commission_id,@network_id=@networkid,@merchant_map_id=@merchantmapid,@transaction_status='payable',@transaction_date=@click_date,@transaction_amount=@order_value,@transaction_commission=@purchaser_revenue,@sub_id=@cu_id
		RETURN;
	END

	BEGIN
	  EXEC comm_GetSubId @subid=@mapsubid OUT,@productid=@productid OUT,@siteid=@siteid OUT,@custom1=@custom1 OUT,@custom2=@custom2 OUT,@custom3=@custom3 OUT,@custom4=@custom4 OUT,@custom5=@custom5 OUT,@iscashback=@iscashback OUT;
	END

--Get Offer
--Get Offer
  DECLARE @commissiongross bit=1,@slmcommissiongross bit=1,@offer_multiplier decimal(18,4)=1,@offer_offertype varchar(255),@offer_slmcommission_percent decimal(18,4)=0,@offer_commission_percent decimal(18,4)=0,@slm_commission decimal(18,4)=0,@slm_expired decimal(18,4)=0
  EXEC comm_GetCommissionSLM @commission=@offer_commission_percent OUT,@offertype=@offer_offertype OUT,@multiplier=@offer_multiplier OUT,@slmcommission=@offer_slmcommission_percent OUT,@slmcommissiongross=@slmcommissiongross OUT,@commissiongross=@commissiongross OUT, @merchantmapid=@merchantmapid,@offerid=@offerid,@record_id=@record_id,@siteid=@siteid;


--End Get Offer

--/* 
	
  declare @multiplied_commission_amount decimal(18,4) = @total_commission * @offer_multiplier;
--Calculate the user's percent of commission the rest goes to remaining for indi
  SET @user_commission=ROUND(@multiplied_commission_amount * @offer_commission_percent, 2);

--If no user in record all goes into remaining
  IF(@cu_id IS NULL OR @cu_id='')
  BEGIN
	SET @user_commission = 0;
	SET @offer_commission_percent=0;
  END
 ELSE
  BEGIN
  --Single Level Marketing
	DECLARE @hostchannelid VARCHAR(255),@expirationdate DateTime,@offersiteid varchar(255);
	BEGIN
		SELECT @hostchannelid=Custom4,@expirationdate=Expiration,@offersiteid=SiteId FROM Management_LinkSubIds WHERE SubId=@cu_id;
	END
	IF(@hostchannelid IS NOT NULL AND @hostchannelid !='')
	BEGIN
		IF(@offer_slmcommission_percent <= 0)
		BEGIN
		 SELECT @offer_slmcommission_percent=SLMDefaultCommissionProducts/100.00 FROM Management_SiteProperties WHERE SiteId=@offersiteid
		END
		IF(@offer_slmcommission_percent > 0)
		BEGIN
			IF(getdate()<=@expirationdate)
			BEGIN
				SET @slm_commission=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
			ELSE IF(@offer_offertype='slm')
			BEGIN
				SET @slm_expired=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
		END
	END
	--End SLM
  END

  SET @remaining_commission = (@multiplied_commission_amount-@user_commission-@slm_commission-@slm_expired);

--Set the status for the transaction (pending->locked->paid)
  SET @transaction_status = 'pending';
  SET @user_status = 'pending';

	--Update if exist else insert into the affiliates feed table
  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	IF EXISTS (SELECT 1 FROM Feed_Commissions_VigLink WHERE record_id = @record_id)
	BEGIN
		IF EXISTS (SELECT 1 FROM Feed_Commissions_VigLink WHERE record_id = @record_id AND commission_payout_id IS NULL)
		BEGIN
--Record is still open so update all fields except original user percent of commission
		  UPDATE Feed_Commissions_VigLink
		  SET 
			commission_id = @offerid,
			commission_date = @commission_date,
			cu_id = @cu_id,
			account_id = @account_id,
			click_url = @click_url,
			campaign_id = @campaign_id,
			device_type = @device_type,
			merchant_group_id = @merchant_group_id,
			merchant_id = @merchant_id,
			merchant_name = @merchant_name,
			merchant_name_original = @merchant_name_original,
			order_value = @order_value,
			purchaser_revenue = @purchaser_revenue,
			networkid=@networkid,
			merchantmapid=@merchantmapid,
			user_status=@user_status,
			transaction_status = @transaction_status,
			user_commission = ROUND(@multiplied_commission_amount * user_commission_percent,2),--on update must use original user percent from insert
			remaining_commission = (@multiplied_commission_amount - ROUND(@multiplied_commission_amount * user_commission_percent,2))--on update must use original user percent from insert
			WHERE record_id=@record_id;
		END
		ELSE
		BEGIN
--Record is closed do to payment so just update status
		UPDATE Feed_Commissions_VigLink
		  SET 
			transaction_status = @transaction_status
			WHERE record_id=@record_id;
		END
	END
	ELSE
	BEGIN
--Record does not exist so insert full record
	  INSERT INTO Feed_Commissions_VigLink(record_id,commission_id,commission_date,cu_id,account_id,click_url,click_date,campaign_id,campaign_name,device_type,merchant_group_id,merchant_id,merchant_name,merchant_name_original,order_value,purchaser_revenue,networkid,transaction_status,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status)
	  VALUES
	  (@record_id,@commission_id,@commission_date,@cu_id,@account_id,@click_url,@click_date,@campaign_id,@campaign_name,@device_type,@merchant_group_id,@merchant_id,@merchant_name,@merchant_name_original,@order_value,@purchaser_revenue,@networkid,@transaction_status,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status);
	END
	COMMIT TRANSACTION;

	BEGIN
		

--Send record to combined commissions table
		EXEC comm_SetCommission 
		    @RecordId = @record_id,
			@TransactionId = @commission_id,
			@NetworkId = @networkid,
			@MerchantMapId = @merchantmapid,
			@TransactionDate = @commission_date,
			@TransactionStatus = @transaction_status,
			@TransactionAmount = @order_value,
			@TransactionCommission = @multiplied_commission_amount,
			@UserId = @mapsubid,
			@UserStatus = @user_status,
			@UserCommissionPercent = @offer_commission_percent,
			@UserCommission = @user_commission,
			@RemainingCommission = @remaining_commission,
			@ProductId=@productid,
			@SiteId=@siteid,
			@Custom1=@custom1,
			@Custom2=@custom2,
			@Custom3=@custom3,
			@Custom4=@custom4,
			@Custom5=@custom5,
			@Quantity=1,
			@SLMCommission=@slm_commission,
			@SLMCommissionExpired=@slm_expired,
			@SLMCommissionPercent=@offer_slmcommission_percent,
			@Sku='',
			@ProductName='',
			@Comment=@campaign_name;
	END
	BEGIN
		DECLARE @EarningsSource varchar(50)='affiliatecommissions';
		IF(@iscashback=1)
		BEGIN
			SET @EarningsSource = 'affiliatecashback';
		END
	END
	BEGIN
		EXEC comm_SetEarnings @record_id, @cu_id, @EarningsSource
	END
--Save to record history for processid

	BEGIN
--Record does not exist so insert full record
	  INSERT INTO Feed_History_VigLink(processid,record_id,commission_id,commission_date,cu_id,account_id,click_url,click_date,campaign_id,campaign_name,device_type,merchant_group_id,merchant_id,merchant_name,merchant_name_original,order_value,purchaser_revenue,networkid,transaction_status,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status)
	  VALUES
	  (@processid,@record_id,@commission_id,@commission_date,@cu_id,@account_id,@click_url,@click_date,@campaign_id,@campaign_name,@device_type,@merchant_group_id,@merchant_id,@merchant_name,@merchant_name_original,@order_value,@purchaser_revenue,@networkid,@transaction_status,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status);
	END
END
