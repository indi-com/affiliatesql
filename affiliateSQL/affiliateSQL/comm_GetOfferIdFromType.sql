/****** Object:  StoredProcedure [dbo].[comm_GetOfferIdFromType]    Script Date: 11/6/2020 11:24:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[comm_GetOfferIdFromType]
(
 @merchantmapid varchar(255)='',
 @offertype varchar(255)='',
 @offerid varchar(50) ='' OUT
)
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @defaultofferid varchar(255);
BEGIN
--Get default offer
	SELECT @offerid=a.offerid FROM Management_MerchantOffers a,Management_Merchants b
	WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid
   AND a.OfferType=@offertype;
END 
RETURN @offerid;
END
