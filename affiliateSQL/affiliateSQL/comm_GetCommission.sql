/****** Object:  StoredProcedure [dbo].[comm_GetCommission]    Script Date: 11/6/2020 11:22:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      BEC
-- Create Date: 10/3/2019
-- Description: Sets user's commission by merchant and offer
-- Sets @output
-- =============================================
ALTER PROCEDURE [dbo].[comm_GetCommission]
(
	@output decimal(18,4) OUT,
    @merchantmapid varchar(255) = '',
	@offerid varchar(255) = '',
	@multiplier decimal(18,4) = 1 OUT
)
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @default decimal(18,4),@defaultofferid varchar(255),@brand decimal(18,4),@brandofferid VARCHAR(255);
BEGIN
--Get default offer
	SELECT @default=a.Commission,@defaultofferid=a.OfferId,@multiplier=a.Multiplier FROM Management_MerchantOffers a,Management_Merchants b
	WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid
   AND OfferType='default';
END
BEGIN
--Get brand offer
	SELECT @brand=a.Commission,@brandofferid=a.OfferId,@multiplier=a.Multiplier FROM Management_MerchantOffers a,Management_Merchants b
	WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid AND OfferId=@offerid
	 AND OfferType!='default';
END
--If default is null set to our default of 70%
IF(@default  IS NULL)
BEGIN
	SET @output = 70.00/100.00;
END
ELSE
BEGIN
	SET @output = @default/100.00;
END
--Override with brand offer if brandoffer id = offerid and defaultofferid != offerid sent
IF(@brandofferid IS NOT NULL)
BEGIN
	IF(@brandofferid = @offerid AND @offerid != @defaultofferid)
	BEGIN
		IF(@brand IS NOT NULL)
		BEGIN
			SET @output=@brand/100.00;
		END
	END
END
--Fix output if output is out of range
IF(@output<0)
BEGIN
	SET @output=0;
END
ELSE IF(@output>1)
BEGIN
	SET @output=1;
END
IF(@multiplier  IS NULL)
BEGIN
 SET @multiplier=1;
END
return;
END
