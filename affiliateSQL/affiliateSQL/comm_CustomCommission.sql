/****** Object:  StoredProcedure [dbo].[comm_CustomCommission]    Script Date: 9/28/2021 1:16:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
ALTER PROCEDURE [dbo].[comm_CustomCommission]
(
	@record_id varchar(255),
	@transaction_id varchar(255),
	@network_id varchar(50),
	@merchant_map_id varchar(255) = NULL,
	@transaction_status varchar(255) = NULL,
	@transaction_date datetime = NULL,
	@transaction_amount decimal(18,4) = NULL,
	@transaction_commission decimal(18,4) = NULL,
	@sub_id varchar(255) = NULL
)
AS
BEGIN
 SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	DECLARE @subid varchar(255), @siteid varchar(255)
	DECLARE @customer_commission decimal(18,4)=0,@indi_commission decimal(18,4)=0
	DECLARE @status varchar(255)
	SELECT @status = case @transaction_status
	WHEN 'locked' THEN 'pending'
	WHEN 'voided' THEN 'voided'
	WHEN 'payable' THEN 'payable'
	ELSE 'pending'
	END
	
	BEGIN
		select @siteid = s from dbo.SplitString(@sub_id,'_') where zeroBasedOccurance=1
		select @subid = s from dbo.SplitString(@sub_id,'_') where zeroBasedOccurance=2
	END
	BEGIN
		SET @customer_commission=ROUND(@transaction_commission*.85,2)
		SET @indi_commission=@transaction_commission-@customer_commission
	END
	--If record exist check which update to run
	IF EXISTS (SELECT 1 FROM Custom_Commissions WHERE RecordId = @record_id)
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM Custom_Commissions WHERE RecordId = @record_id AND TransactionStatus=@status AND TransactionAmount=@transaction_amount AND TransactionCommission=@transaction_commission)
		BEGIN
			UPDATE Custom_Commissions SET 
				TransactionStatus=@status,
				TransactionAmount=@transaction_amount,
				TransactionCommission=@transaction_commission,
				CustomerCommission=@customer_commission,
				IndiCommission=@indi_commission,
				LastUpdateDate=GETDATE()
				WHERE RecordId=@record_id;
		END
	END
	ELSE
	BEGIN
		INSERT INTO Custom_Commissions(RecordId,TransactionId,NetworkId,MerchantMapId,TransactionDate,TransactionStatus,TransactionAmount,TransactionCommission,SubId,LastUpdateDate,siteid,CustomerCommission,IndiCommission)
		VALUES
		(@record_id,@transaction_id,@network_id,@merchant_map_id,@transaction_date,@status,@transaction_amount,@transaction_commission,@subid,GETDATE(),@siteid,@customer_commission,@indi_commission);
	END
	COMMIT TRANSACTION;
END