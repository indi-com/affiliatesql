/****** Object:  StoredProcedure [dbo].[comm_GetCommissionByOfferType]    Script Date: 11/6/2020 11:22:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      BEC
-- Create Date: 10/3/2019
-- Description: Sets user's commission by merchant and offer
-- Sets @output
-- =============================================
ALTER PROCEDURE [dbo].[comm_GetCommissionByOfferType]
(
	@output decimal(18,4) OUT,
    @merchantmapid varchar(255) = '',
	@offertype varchar(255) = '',
	@multiplier int =1 OUT
)
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @default decimal(18,4),@defaultofferid varchar(255);
BEGIN
--Get default offer
	SELECT @default=a.Commission,@multiplier=a.Multiplier FROM Management_MerchantOffers a,Management_Merchants b
	WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid
   AND a.OfferType=@offertype;
END

--If default is null set to our default of 70%
IF(@default  IS NULL)
BEGIN
	SET @output = 70.00/100.00;
END
ELSE
BEGIN
	SET @output = @default/100.00;
END

--Fix output if output is out of range
IF(@output<0)
BEGIN
	SET @output=0;
END
ELSE IF(@output>1)
BEGIN
	SET @output=1;
END
if(@multiplier is null)
begin
 set @multiplier=1;
end
return;
END
