/****** Object:  StoredProcedure [dbo].[comm_GetCommissionSLM]    Script Date: 11/6/2020 11:23:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      BEC
-- Create Date: 10/3/2019
-- Description: Sets user's commission by merchant and offer
-- Sets @output
-- =============================================
ALTER PROCEDURE [dbo].[comm_GetCommissionSLM]
(
	@commission decimal(18,4)=0 OUT,
    @offertype varchar(255)='' OUT,
	@multiplier decimal(18,4) = 1 OUT,
	@slmcommission decimal(18,4)=0 OUT,
	@slmcommissiongross bit=1 OUT,
	@commissiongross bit=1 OUT,
	@merchantmapid varchar(255) = '',
	@offerid varchar(255) = '',
	@record_id varchar(255)='',
	@siteid varchar(50)
)
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @default decimal(18,4),@defaultofferid varchar(255)='' ,@defaultmultiplier decimal(18,4),@defaultoffertype varchar(255),@defaultslmcommission decimal(18,4)=0;
	DECLARE @brand decimal(18,4),@brandofferid VARCHAR(255)='' ,@brandmultiplier decimal(18,4),@brandoffertype varchar(255),@brandslmcommission decimal(18,4)=0;
	DECLARE @slm decimal(18,4),@slmofferid VARCHAR(255)='',@slmmultiplier decimal(18,4),@slmoffertype varchar(255),@slmslmcommission decimal(18,4)=0;

	DECLARE @override_commission decimal(18,4)=0,@override_slmcommission decimal(18,4)=0,@override_slmcommissiongross bit=0,@override_commissiongross bit=0;
BEGIN
	IF EXISTS (SELECT 1 FROM Management_CommissionOffers WHERE record_id = @record_id)
	BEGIN
		 SELECT @commission=commission,@offertype=offertype,@multiplier=multiplier,@slmcommission=slmcommission,@commissiongross=commissiongross,@slmcommissiongross=slmcommissiongross
		 FROM Management_CommissionOffers
		 WHERE record_id=@record_id
	END
	ELSE
	BEGIN
		--Get default offer
		IF EXISTS (SELECT 1 FROM Management_MerchantOffers WHERE SiteId = @siteid and OfferType='sitedefault' and OfferId=@offerid)
			BEGIN
					SELECT @default=a.Commission,@defaultofferid=a.OfferId,@defaultmultiplier=a.Multiplier,@defaultoffertype=a.OfferType,
					@defaultslmcommission=a.SLMCommission FROM Management_MerchantOffers a,Management_Merchants b
					WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid and a.SiteId=@siteid
					AND OfferType='sitedefault';
			END
		ELSE
			BEGIN
					SELECT @default=a.Commission,@defaultofferid=a.OfferId,@defaultmultiplier=a.Multiplier,@defaultoffertype=a.OfferType,
					@defaultslmcommission=a.SLMCommission FROM Management_MerchantOffers a,Management_Merchants b
					WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid
					AND OfferType='default';
			END
		BEGIN
		--Get brand offer
			SELECT @brand=a.Commission,@brandofferid=a.OfferId,@brandmultiplier=a.Multiplier,@brandoffertype=a.OfferType,@brandslmcommission=a.SLMCommission FROM Management_MerchantOffers a,Management_Merchants b
			WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid AND OfferId=@offerid
			 AND OfferType='brandambassador';
		END
		--Get slm offer
		IF EXISTS (SELECT 1 FROM Management_MerchantOffers WHERE SiteId = @siteid and OfferType='siteslm' and OfferId=@offerid)
			BEGIN
				SELECT @slm=a.Commission,@slmofferid=a.OfferId,@slmmultiplier=a.Multiplier,@slmoffertype=a.OfferType,@slmslmcommission=a.SLMCommission FROM Management_MerchantOffers a,Management_Merchants b
				WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid and a.SiteId=@siteid
				 AND OfferType='siteslm';
			END
		ELSE
			BEGIN
				SELECT @slm=a.Commission,@slmofferid=a.OfferId,@slmmultiplier=a.Multiplier,@slmoffertype=a.OfferType,@slmslmcommission=a.SLMCommission FROM Management_MerchantOffers a,Management_Merchants b
				WHERE b.Id=a.ManagementMerchantId AND b.MerchantMapId=@merchantmapid AND OfferId=@offerid
				 AND OfferType='slm';
			END
		--If default is null set to our default of 70%
		IF(@defaultofferid IS NULL)
		BEGIN
			SET @commission = 70.00/100.00;
			SET @multiplier=1;
			SET @offertype = 'default';
			SET @slmcommission=0;
		END
		ELSE
		BEGIN
			SET @commission = @default/100.00;
			SET @multiplier=@defaultmultiplier;
			SET @offertype = @defaultoffertype;
			SET @slmcommission = @defaultslmcommission/100;
		END
		--Override with brand offer if brandoffer id = offerid and defaultofferid != offerid sent
		IF(@brandofferid is not null and @brandofferid !='')
		BEGIN
			IF(@brandofferid = @offerid AND @offerid != @defaultofferid)
			BEGIN
				IF(@brand IS NOT NULL)
				BEGIN
					SET @commission=@brand/100.00;
					SET @multiplier = @brandmultiplier;
					SET @offertype = @brandoffertype;
					SET @slmcommission=@brandslmcommission/100;
				END
			END
		END
		IF(@slmofferid is not null and @slmofferid !='')
		BEGIN
			IF(@slmofferid = @offerid AND @offerid != @defaultofferid)
			BEGIN
				IF(@slm IS NOT NULL)
				BEGIN
					SET @commission=@slm/100.00;
					SET @multiplier = @slmmultiplier;
					SET @offertype = @slmoffertype;
					SET @slmcommission=@slmslmcommission/100.00;
				END
			END
		END
		--Fix output if output is out of range
		IF(@commission<0)
		BEGIN
			SET @commission=0;
		END
		ELSE IF(@commission>1)
		BEGIN
			SET @commission=1;
		END
		IF(@multiplier<1)
		BEGIN
		 SET @multiplier=1;
		END
		BEGIN
			SELECT @override_commission=ISNULL(UserPercent,0),@override_commissiongross=ISNULL(UserPercentGross,0),@override_slmcommission=ISNULL(SlmPercent,0),@override_slmcommissiongross=ISNULL(SlmPercentGross,0) FROM Management_SiteEarningsProfiles where SiteId=@siteid and ItemType='affiliatecommissions'
		END
	
		BEGIN
		  --SET TO SITE OVERRIDE WHEN NEEDED
		  IF(@override_commission>0)
		  BEGIN
			SET @commission=@override_commission/100;
			SET @commissiongross=@override_commissiongross;
		  END
		  IF(@override_slmcommission>0)
		  BEGIN
			SET @slmcommission=@override_slmcommission/100;
			SET @slmcommissiongross=@override_slmcommissiongross;
		  END
		END
		
		BEGIN
			INSERT INTO Management_CommissionOffers(record_id,offertype,commission,multiplier,slmcommission,slmcommissiongross,commissiongross)values(@record_id,@offertype,@commission,@multiplier,@slmcommission,@slmcommissiongross,@commissiongross);
		END
	END
END
return;
END
