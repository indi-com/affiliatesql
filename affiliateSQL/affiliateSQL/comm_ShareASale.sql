/****** Object:  StoredProcedure [dbo].[comm_ShareASale]    Script Date: 9/21/2021 10:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
ALTER PROCEDURE [dbo].[comm_ShareASale]
(
	@trans_id varchar(255),
	@user_id varchar(255)= NULL,
	@merchant_id varchar(255)= NULL,
	@trans_date datetime = NULL,
	@trans_amount decimal(18,4) = NULL,
	@commission decimal(18,4) = NULL,
	@comment varchar(max) = NULL,
	@voided int = 0,
	@pending_date datetime = NULL,
	@locked int = 0,
	@aff_comment varchar(255) = NULL,
	@banner_page varchar(max) = NULL,
	@reversal_date datetime = NULL,
	@click_date datetime = NULL,
	@banner_id varchar(255) = NULL,--OFFERID
	@sku_list varchar(255) = NULL,
	@quantity_list varchar(255) = NULL,
	@lock_date datetime = NULL,
	@paid_date datetime = NULL,
	@merchant_organization varchar(1000) = NULL,
	@merchant_website varchar(max) = NULL,
	@trans_type varchar(255) = NULL,
	@merchant_defined_type varchar(1000) = NULL,
	@networkid varchar(255)= NULL,
	@merchantmapid varchar(100) = NULL,
	@processid varchar(255) = NULL,
	@productnames varchar(2000) = NULL
)
AS
BEGIN
--Create unique record id for insert/update
  DECLARE @record_id varchar(255) = @trans_id;
 --Declare standard variables
  DECLARE @user_commission decimal(18,4),@remaining_commission decimal(18,4),@user_status varchar(50),@transaction_status varchar(255);


  IF(@paid_date IS NOT NULL)
  BEGIN
	SET @transaction_status = 'paid';
  END
  ELSE
  BEGIN
	SET @transaction_status = 'pending';
  END
  IF(@voided=1)
  BEGIN
	SET @transaction_status = 'voided';
  END

  IF LEFT(@aff_comment, 4) = 'cus_' 
  BEGIN
	EXEC comm_CustomCommission @record_id=@record_id,@transaction_id=@trans_id,@network_id=@networkid,@merchant_map_id=@merchantmapid,@transaction_status=@transaction_status,@transaction_date=@trans_date,@transaction_amount=@trans_amount,@transaction_commission=@commission,@sub_id=@aff_comment
	RETURN;
  END
 
	DECLARE @custom1 varchar(255) = null,@custom2 varchar(255) = null,@custom3 varchar(255) = null,@custom4 varchar(255) = null,@custom5 varchar(255) = null,@iscashback bit = 0;
	DECLARE @mapsubid varchar(255) = @aff_comment;
	DECLARE @productid int = null,@siteid varchar(255) = null;
	BEGIN
		EXEC comm_GetSubId @subid=@mapsubid OUT,@productid=@productid OUT,@siteid=@siteid OUT,@custom1=@custom1 OUT,@custom2=@custom2 OUT,@custom3=@custom3 OUT,@custom4=@custom4 OUT,@custom5=@custom5 OUT,@iscashback=@iscashback OUT;
	END

--Get Offer
--Get Offer
  DECLARE @commissiongross bit=1,@slmcommissiongross bit=1,@offer_multiplier decimal(18,4)=1,@offer_offertype varchar(255),@offer_slmcommission_percent decimal(18,4)=0,@offer_commission_percent decimal(18,4)=0,@slm_commission decimal(18,4)=0,@slm_expired decimal(18,4)=0
  EXEC comm_GetCommissionSLM @commission=@offer_commission_percent OUT,@offertype=@offer_offertype OUT,@multiplier=@offer_multiplier OUT,@slmcommission=@offer_slmcommission_percent OUT,@slmcommissiongross=@slmcommissiongross OUT,@commissiongross=@commissiongross OUT, @merchantmapid=@merchantmapid,@offerid=@banner_id,@record_id=@record_id,@siteid=@siteid;

--End Get Offer

--/* 

  declare @multiplied_commission_amount decimal(18,4) = @commission * @offer_multiplier;

--Calculate the user's percent of commission the rest goes to remaining for indi
  SET @user_commission=ROUND(@multiplied_commission_amount * @offer_commission_percent, 2);
 
--If no user in record all goes into remaining
  IF(@aff_comment IS NULL OR @aff_comment='')
  BEGIN
	SET @user_commission = 0;
	SET @offer_commission_percent=0;
  END
  ELSE
  BEGIN
  --Single Level Marketing
	DECLARE @hostchannelid VARCHAR(255),@expirationdate DateTime,@offersiteid varchar(255);
	BEGIN
		SELECT @hostchannelid=Custom4,@expirationdate=Expiration,@offersiteid=SiteId FROM Management_LinkSubIds WHERE SubId=@aff_comment;
	END
	IF(@hostchannelid IS NOT NULL AND @hostchannelid !='')
	BEGIN
		IF(@offer_slmcommission_percent <= 0)
		BEGIN
		 SELECT @offer_slmcommission_percent=SLMDefaultCommissionProducts/100.00 FROM Management_SiteProperties WHERE SiteId=@offersiteid
		END
		IF(@offer_slmcommission_percent > 0)
		BEGIN
			IF(getdate()<=@expirationdate)
			BEGIN
				SET @slm_commission=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
			ELSE IF(@offer_offertype='slm')
			BEGIN
				SET @slm_expired=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
		END
	END
	--End SLM
  END

  SET @remaining_commission = (@multiplied_commission_amount-@user_commission-@slm_commission-@slm_expired);

--Set the status for the transaction (pending->locked->paid)
  IF(@paid_date IS NOT NULL)
  BEGIN
	SET @transaction_status = 'paid';
	SET @user_status = 'payable';
  END
  ELSE
  BEGIN
	SET @transaction_status = 'pending';
	SET @user_status = 'pending';
  END
  IF(@voided=1)
  BEGIN
	SET @transaction_status = 'voided';
	SET @user_status = 'voided';
  END
 --Update if exist else insert into the affiliates feed table
  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	IF EXISTS (SELECT 1 FROM Feed_Commissions_ShareASale WHERE record_id = @record_id)
	BEGIN
		IF EXISTS (SELECT 1 FROM Feed_Commissions_ShareASale WHERE record_id = @record_id AND commission_payout_id IS NULL)
		BEGIN
--Record is still open so update all fields except original user percent of commission
		  UPDATE Feed_Commissions_ShareASale
			  SET 
				[user_id] = @user_id,
				merchant_id = @merchant_id,
				trans_date = @trans_date,
				trans_amount = @trans_amount,
				commission=@commission,
				comment = @comment,
				voided = @voided,
				pending_date = @pending_date,
				locked = @locked,
				aff_comment = @aff_comment,
				banner_page = @banner_page,
				reversal_date = @reversal_date,
				click_date = @click_date,
				banner_id = @banner_id,
				sku_list = @sku_list,
				quantity_list = @quantity_list,
				lock_date = @lock_date,
				paid_date = @paid_date,
				merchant_organization=@merchant_organization,
				merchant_website=@merchant_website,
				trans_type = @trans_type,
				merchant_defined_type = @merchant_defined_type,
				networkid = @networkid,
				merchantmapid=@merchantmapid,
				user_status=@user_status,
				transaction_status = @transaction_status,
				user_commission = ROUND(@multiplied_commission_amount * user_commission_percent,2),--on update must use original user percent from insert
				remaining_commission = (@multiplied_commission_amount - ROUND(@multiplied_commission_amount * user_commission_percent,2))--on update must use original user percent from insert
			WHERE record_id=@record_id;
		END
		ELSE
		BEGIN
--Record is closed do to payment so just update status
		UPDATE Feed_Commissions_ShareASale
		  SET 
			transaction_status= @transaction_status
			WHERE record_id=@record_id;
		END
	END
	ELSE
	BEGIN
--Record does not exist so insert full record
	  INSERT INTO Feed_Commissions_ShareASale(record_id,trans_id,[user_id],merchant_id,trans_date,trans_amount,commission,comment,voided,pending_date,locked,aff_comment,banner_page,reversal_date,click_date,banner_id,sku_list,quantity_list,lock_date,paid_date,merchant_organization,merchant_website,trans_type,merchant_defined_type,networkid,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status,transaction_status,multiplier)
	  VALUES
	  (@record_id,@trans_id,@user_id,@merchant_id,@trans_date,@trans_amount,@commission,@comment,@voided,@pending_date,@locked,@aff_comment,@banner_page,@reversal_date,@click_date,@banner_id,@sku_list,@quantity_list,@lock_date,@paid_date,@merchant_organization,@merchant_website,@trans_type,@merchant_defined_type,@networkid,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status,@transaction_status,@offer_multiplier);
	END
	COMMIT TRANSACTION;

	BEGIN
		
--Get sum quantity from quantity_list		
DECLARE @q_items TABLE (items INT)
INSERT INTO @q_items SELECT * FROM STRING_SPLIT(REPLACE(@quantity_list, '%2C', ','), ',')
DECLARE @quantity INT = (SELECT CASE WHEN SUM(items) = 0 THEN 1 ELSE SUM(items) END FROM @q_items)

--Send record to combined commissions table
		EXEC comm_SetCommission 
		    @RecordId = @record_id,
			@TransactionId = @trans_id,
			@NetworkId = @networkid,
			@MerchantMapId = @merchantmapid,
			@TransactionDate = @trans_date,
			@TransactionStatus = @transaction_status,
			@TransactionAmount = @trans_amount,
			@TransactionCommission = @multiplied_commission_amount,
			@UserId = @mapsubid,
			@UserStatus = @user_status,
			@UserCommissionPercent = @offer_commission_percent,
			@UserCommission = @user_commission,
			@RemainingCommission = @remaining_commission,
			@ProductId=@productid,
			@SiteId=@siteid,
			@Custom1=@custom1,
			@Custom2=@custom2,
			@Custom3=@custom3,
			@Custom4=@custom4,
			@Custom5=@custom5,
			@Quantity=@quantity,
			@SLMCommission=@slm_commission,
			@SLMCommissionExpired=@slm_expired,
			@SLMCommissionPercent=@offer_slmcommission_percent,
			@Sku=@sku_list,
			@ProductName=@productnames,
			@Comment=@comment;
	END
	BEGIN
		DECLARE @EarningsSource varchar(50)='affiliatecommissions';
		IF(@iscashback=1)
		BEGIN
			SET @EarningsSource = 'affiliatecashback';
		END
	END
	BEGIN
		EXEC comm_SetEarnings @record_id, @aff_comment, @EarningsSource
	END
--Save to record history for processid
	BEGIN
		INSERT INTO Feed_History_ShareASale(record_id,processid,trans_id,[user_id],merchant_id,trans_date,trans_amount,commission,comment,voided,pending_date,locked,aff_comment,banner_page,reversal_date,click_date,banner_id,sku_list,quantity_list,lock_date,paid_date,merchant_organization,merchant_website,trans_type,merchant_defined_type,networkid,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status,transaction_status)
		VALUES(@record_id,@processid,@trans_id,@user_id,@merchant_id,@trans_date,@trans_amount,@commission,@comment,@voided,@pending_date,@locked,@aff_comment,@banner_page,@reversal_date,@click_date,@banner_id,@sku_list,@quantity_list,@lock_date,@paid_date,@merchant_organization,@merchant_website,@trans_type,@merchant_defined_type,@networkid,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status,@transaction_status);
	END

	--*/
END
