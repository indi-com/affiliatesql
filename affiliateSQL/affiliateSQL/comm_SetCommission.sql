/****** Object:  StoredProcedure [dbo].[comm_SetCommission]    Script Date: 11/6/2020 11:27:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      BEC
-- Create Date: 10/3/2019
-- Description: Insert or Update network commissions in combined (Management_Commissions) table
-- =============================================
ALTER PROCEDURE [dbo].[comm_SetCommission]
(
	@RecordId varchar(255),
	@TransactionId varchar(255),
	@NetworkId varchar(50) = NULL,
	@MerchantMapId varchar(255) = NULL,
	@TransactionDate datetime = NULL,
	@TransactionStatus varchar(255) = NULL,
	@TransactionAmount decimal(18,4) = NULL,
	@TransactionCommission decimal(18,4) = NULL,
	@UserId varchar(255) = NULL,
	@UserStatus varchar(50)=NULL,
	@UserCommissionPercent decimal(18,4) = NULL,
	@UserCommission decimal(18,4) = NULL,
	@RemainingCommission decimal(18,4) = NULL,
	@ProductId int = NULL,
	@SiteId varchar(255) = NULL,
	@Custom1 varchar(255) = NULL,
	@Custom2 varchar(255) = NULL,
	@Custom3 varchar(255) = NULL,
	@Custom4 varchar(255) = NULL,
	@Custom5 varchar(255) = NULL,
	@Quantity int = 1,
	@SLMCommission decimal(18,4) = 0,
	@SLMCommissionExpired decimal(18,4) = 0,
	@SLMCommissionPercent decimal(18,4) = 0,
	@Sku varchar(500) = NULL,
	@ProductName varchar(2000) = NULL,
	@Comment varchar(2000) = NULL
)
AS
BEGIN


  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	--If record exist check which update to run
	IF EXISTS (SELECT 1 FROM Management_Commissions WHERE RecordId = @RecordId)
	BEGIN
	--If status paid only update transaction status all others are locked in do to payment
		IF EXISTS (SELECT 1 FROM Management_Commissions WHERE RecordId = @RecordId AND (UserStatus = 'paid' OR SLMUserStatus = 'paid' OR  ManualOverride=1 OR LockedForPayout=1))
		BEGIN
			UPDATE Management_Commissions
			SET TransactionStatus=@TransactionStatus
			WHERE RecordId = @RecordId;
		END
		ELSE
		--Else: Not status paid, record is still open so update full record
		BEGIN
			UPDATE Management_Commissions
			SET TransactionStatus=@TransactionStatus,
			TransactionDate=@TransactionDate,
			TransactionAmount = @TransactionAmount,
			TransactionCommission=@TransactionCommission,
			UserId=@UserId,
			UserStatus=@UserStatus,
			SLMUserStatus = @UserStatus,
			UserCommissionPercent=@UserCommissionPercent,
			UserCommission=@UserCommission,
			RemainingCommission=@RemainingCommission,
			--Custom1=@Custom1,
			--Custom2=@Custom2,
			--Custom3=@Custom3,
			--Custom4=@Custom4,
			--Custom5=@Custom5,
			SiteId=@SiteId,
			ProductId=@ProductId,
			Quantity=@Quantity,
			SLMCommission=@SLMCommission,
			SLMCommissionExpired=@SLMCommissionExpired,
			SLMCommissionPercent=@SLMCommissionPercent,
			SkuList=@Sku,
			ProductNames=@ProductName,
			Comment=@Comment
			WHERE RecordId = @RecordId;
		END
	END
	ELSE
	--Else: No record so insert
	BEGIN
	  INSERT INTO Management_Commissions(RecordId,TransactionId,NetworkId,MerchantMapId,TransactionDate,TransactionStatus,TransactionAmount,TransactionCommission,UserId,UserStatus,UserCommissionPercent,UserCommission,RemainingCommission,SiteId,ProductId,Custom1,Custom2,Custom3,Custom4,Custom5,Quantity,SLMCommission,SLMCommissionExpired,SLMUserStatus,SLMCommissionPercent,SkuList,ProductNames,Comment)
	  VALUES
	  (@RecordId,@TransactionId,@NetworkId,@MerchantMapId,@TransactionDate,@TransactionStatus,@TransactionAmount,@TransactionCommission,@UserId,@UserStatus,@UserCommissionPercent,@UserCommission,@RemainingCommission,@SiteId,@ProductId,@Custom1,@Custom2,@Custom3,@Custom4,@Custom5,@Quantity,@SLMCommission,@SLMCommissionExpired,@UserStatus,@SLMCommissionPercent,@Sku,@ProductName,@Comment);
	END
	COMMIT TRANSACTION;
	END
