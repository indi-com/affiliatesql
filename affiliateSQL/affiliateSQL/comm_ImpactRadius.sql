/****** Object:  StoredProcedure [dbo].[comm_ImpactRadius]    Script Date: 9/21/2021 10:29:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      BEC
-- Create Date: 10/3/2019
-- Description: Process single record from pepperjam feed
-- =============================================
ALTER PROCEDURE [dbo].[comm_ImpactRadius]
(
	@referral_date datetime = NULL,
	@action_date datetime = NULL,
	@locking_date datetime = NULL,
	@action_id varchar(50) = NULL,
	@merchant_id varchar(50) = NULL,
	@offer_id varchar(50) = NULL,
	@campaign varchar(50) = NULL,
	@action_tracker varchar(50) = NULL,
	@status varchar(50) = NULL,
	@status_detail varchar(500) = NULL,
	@sku varchar(50) = NULL,
	@item_name varchar(50) = NULL,
	@category varchar(50) = NULL,
	@quantity int = NULL,
	@sale_amount decimal(18,4) = NULL,
	@original_sale_amount decimal(18,4) = NULL,
	@payout decimal(18,4) = NULL,
	@original_payout decimal(18,4) = NULL,
	@promo_code varchar(50) = NULL,
	@ad varchar(50) = NULL,
	@referring_url varchar(2000) = NULL,
	@referral_type varchar(50) = NULL,
	@geo_location varchar(255) = NULL,
	@sub_id varchar(50) = NULL,
	@device varchar(50) = NULL,
	@os varchar(50) = NULL,
	@user_agent varchar(50) = NULL,
	@networkid varchar(255) = NULL,
	@merchantmapid varchar(100) = NULL,
	@processid varchar(255) = NULL
)
AS
BEGIN

DECLARE @custom1 varchar(255) = null,@custom2 varchar(255) = null,@custom3 varchar(255) = null,@custom4 varchar(255) = null,@custom5 varchar(255) = null,@iscashback bit = 0;
	DECLARE @mapsubid varchar(255) = @sub_id;
	DECLARE @productid int = null,@siteid varchar(255) = null;
	--Create unique record id for insert/update
  DECLARE @record_id varchar(255) = @action_id+'_'+@sku;
   --Declare standard variables
  DECLARE @user_commission decimal(18,4),@remaining_commission decimal(18,4),@user_status varchar(50),@transaction_status varchar(255);

  --Set the status for the transaction (pending->approved)
  SET @transaction_status = LOWER(@status);

--Set the status for the user (pending->locked->payable->paid)
  SELECT @user_status = case @transaction_status
	WHEN 'approved' THEN 'payable'
	ELSE 'pending'
	END

	IF LEFT(@sub_id, 4) = 'cus_' 
	BEGIN
		EXEC comm_CustomCommission @record_id=@record_id,@transaction_id=@action_id,@network_id=@networkid,@merchant_map_id=@merchantmapid,@transaction_status=@transaction_status,@transaction_date=@action_date,@transaction_amount=@sale_amount,@transaction_commission=@payout,@sub_id=@sub_id
		RETURN;
	END

	BEGIN
	  EXEC comm_GetSubId @subid=@mapsubid OUT,@productid=@productid OUT,@siteid=@siteid OUT,@custom1=@custom1 OUT,@custom2=@custom2 OUT,@custom3=@custom3 OUT,@custom4=@custom4 OUT,@custom5=@custom5 OUT,@iscashback=@iscashback OUT;
	END

--Get user's offerid by type
  DECLARE @offer_type varchar(255) = 'default';
  IF(@offer_id='408261')
  BEGIN
	SET @offer_type = 'brandambassador';
  END
  declare @found_offer_id varchar(255)='';
  EXEC comm_GetOfferIdFromType @merchantmapid, @offer_type,@found_offer_id OUT;

 
--Get Offer
 --Get Offer
  DECLARE @commissiongross bit=1,@slmcommissiongross bit=1,@offer_multiplier decimal(18,4)=1,@offer_offertype varchar(255),@offer_slmcommission_percent decimal(18,4)=0,@offer_commission_percent decimal(18,4)=0,@slm_commission decimal(18,4)=0,@slm_expired decimal(18,4)=0
  EXEC comm_GetCommissionSLM @commission=@offer_commission_percent OUT,@offertype=@offer_offertype OUT,@multiplier=@offer_multiplier OUT,@slmcommission=@offer_slmcommission_percent OUT,@slmcommissiongross=@slmcommissiongross OUT,@commissiongross=@commissiongross OUT, @merchantmapid=@merchantmapid,@offerid=@found_offer_id,@record_id=@record_id,@siteid=@siteid;

--End Get Offer
  declare @multiplied_commission_amount decimal(18,4) = @payout * @offer_multiplier;
  --Calculate the user's percent of commission the rest goes to remaining for indi
  SET @user_commission=ROUND(@multiplied_commission_amount * @offer_commission_percent, 2);

--If no user in record all goes into remaining
  IF(@sub_id IS NULL OR @sub_id='')
  BEGIN
	SET @user_commission = 0;
	SET @offer_commission_percent=0;
  END
    ELSE
  BEGIN
  --Single Level Marketing
	DECLARE @hostchannelid VARCHAR(255),@expirationdate DateTime,@offersiteid varchar(255);
	BEGIN
		SELECT @hostchannelid=Custom4,@expirationdate=Expiration,@offersiteid=SiteId FROM Management_LinkSubIds WHERE SubId=@sub_id;
	END
	IF(@hostchannelid IS NOT NULL AND @hostchannelid !='')
	BEGIN
		IF(@offer_slmcommission_percent <= 0)
		BEGIN
		 SELECT @offer_slmcommission_percent=SLMDefaultCommissionProducts/100.00 FROM Management_SiteProperties WHERE SiteId=@offersiteid
		END
		IF(@offer_slmcommission_percent > 0)
		BEGIN
			IF(getdate()<=@expirationdate)
			BEGIN
				SET @slm_commission=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
			ELSE IF(@offer_offertype='slm')
			BEGIN
				SET @slm_expired=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
		END
	END
	--End SLM
  END

  SET @remaining_commission = (@multiplied_commission_amount-@user_commission-@slm_commission-@slm_expired);



--Update if exist else insert into the affiliates feed table
  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	IF EXISTS (SELECT 1 FROM Feed_Commissions_ImpactRadius WHERE record_id = @record_id)
	BEGIN
		IF EXISTS (SELECT 1 FROM Feed_Commissions_ImpactRadius WHERE record_id = @record_id AND commission_payout_id IS NULL)
		BEGIN
--Record is still open so update all fields except original user percent of commission
		  UPDATE Feed_Commissions_ImpactRadius
		  SET 
			referral_date = @referral_date,
			action_date = @action_date,
			locking_date = @locking_date,
			action_id=@action_id,
			merchant_id=@merchant_id,
			offer_id=@offer_id,
			campaign = @campaign,
			action_tracker=@action_tracker,
			[status] = @status,
			status_detail = @status_detail,
			sku = @sku,
			item_name=@item_name,
			category = @category,
			quantity = @quantity,
			sale_amount = @sale_amount,
			original_sale_amount = @original_sale_amount,
			payout = @payout,
			original_payout = @original_payout,
			promo_code = @promo_code,
			ad = @ad,
			referring_url = @referring_url,
			referral_type = @referral_type,
			geo_location = @geo_location,
			sub_id = @sub_id,
			device = @device,
			os = @os,
			user_agent = @user_agent,
			networkid=@networkid,
			merchantmapid=@merchantmapid,
			user_status=@user_status,
			transaction_status = @transaction_status,
			user_commission = ROUND(@multiplied_commission_amount * user_commission_percent,2),--on update must use original user percent from insert
			remaining_commission = (@multiplied_commission_amount - ROUND(@multiplied_commission_amount * user_commission_percent,2))--on update must use original user percent from insert
			WHERE record_id=@record_id;
		END
		ELSE
		BEGIN
--Record is closed do to payment so just update status
		UPDATE Feed_Commissions_PepperJam
		  SET 
			[status] = @status
			WHERE record_id=@record_id;
		END
	END
	ELSE
	BEGIN
--Record does not exist so insert full record
	  INSERT INTO Feed_Commissions_ImpactRadius(record_id,referral_date,action_date,locking_date,action_id,merchant_id,offer_id,campaign,action_tracker,[status],status_detail,sku,item_name,category,quantity,sale_amount,original_sale_amount,payout,original_payout,promo_code,ad,referring_url,referral_type,geo_location,sub_id,device,os,user_agent,networkid,transaction_status,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status)
	  VALUES
	  (@record_id,@referral_date,@action_date,@locking_date,@action_id,@merchant_id,@offer_id,@campaign,@action_tracker,@status,@status_detail,@sku,@item_name,@category,@quantity,@sale_amount,@original_sale_amount,@payout,@original_payout,@promo_code,@ad,@referring_url,@referral_type,@geo_location,@sub_id,@device,@os,@user_agent,@networkid,@transaction_status,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status);
	END
	COMMIT TRANSACTION;
	
	BEGIN
	--Get MapId
	
--Send record to combined commissions table
		EXEC comm_SetCommission 
		    @RecordId = @record_id,
			@TransactionId = @action_id,
			@NetworkId = @networkid,
			@MerchantMapId = @merchantmapid,
			@TransactionDate = @action_date,
			@TransactionStatus = @transaction_status,
			@TransactionAmount = @sale_amount,
			@TransactionCommission = @multiplied_commission_amount,
			@UserId = @mapsubid,
			@UserStatus = @user_status,
			@UserCommissionPercent = @offer_commission_percent,
			@UserCommission = @user_commission,
			@RemainingCommission = @remaining_commission,
			@ProductId=@productid,
			@SiteId=@siteid,
			@Custom1=@custom1,
			@Custom2=@custom2,
			@Custom3=@custom3,
			@Custom4=@custom4,
			@Custom5=@custom5,
			@Quantity=@quantity,
			@SLMCommission=@slm_commission,
			@SLMCommissionExpired=@slm_expired,
			@SLMCommissionPercent=@offer_slmcommission_percent,
			@Sku=@sku,
			@ProductName=@item_name,
			@Comment=@status_detail;
	END
	BEGIN
		DECLARE @EarningsSource varchar(50)='affiliatecommissions';
		IF(@iscashback=1)
		BEGIN
			SET @EarningsSource = 'affiliatecashback';
		END
	END
	BEGIN
		EXEC comm_SetEarnings @record_id, @sub_id, @EarningsSource;
	END
--Save to record history for processid
	BEGIN
		INSERT INTO Feed_History_ImpactRadius(record_id,processid,referral_date,action_date,locking_date,action_id,merchant_id,offer_id,campaign,action_tracker,[status],status_detail,sku,item_name,category,quantity,sale_amount,original_sale_amount,payout,original_payout,promo_code,ad,referring_url,referral_type,geo_location,sub_id,device,os,user_agent,networkid,transaction_status,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status)
		VALUES(@record_id,@processid,@referral_date,@action_date,@locking_date,@action_id,@merchant_id,@offer_id,@campaign,@action_tracker,@status,@status_detail,@sku,@item_name,@category,@quantity,@sale_amount,@original_sale_amount,@payout,@original_payout,@promo_code,@ad,@referring_url,@referral_type,@geo_location,@sub_id,@device,@os,@user_agent,@networkid,@transaction_status,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status);
	END
END
