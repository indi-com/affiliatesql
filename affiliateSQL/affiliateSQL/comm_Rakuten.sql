/****** Object:  StoredProcedure [dbo].[comm_Rakuten]    Script Date: 9/21/2021 10:29:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
ALTER PROCEDURE [dbo].[comm_Rakuten]
(
	@transactionid varchar(255),
	@siteid varchar(255) = NULL,
	@orderid varchar(255) = NULL,
	@transaction_date datetime = NULL,
	@merchantid varchar(255) = NULL,
	@advertiser_name varchar(1000) = NULL,
	@memberid varchar(255) = NULL,
	@sku varchar(255) = NULL,
	@product_name varchar(1000) = NULL,
	@sales decimal(18,4) = NULL,
	@gross_sales decimal(18,4) = NULL,
	@total_commission decimal(18,4) = NULL,
	@adjusted_commission decimal(18,4) = NULL,
	@baseline_commission decimal(18,4) = NULL,
	@gross_commission decimal(18,4) = NULL,
	@gross_total_commission decimal(18,4) = NULL,
	@number_of_items int = NULL,
	@number_of_cancelled_items int = NULL,
	@number_of_net_items int = NULL,
	@referrer_url varchar(max) = NULL,
	@networkid varchar(255)= NULL,
	@merchantmapid varchar(100) = NULL,
	@offerid varchar(255) = NULL,
	@status varchar(255) = NULL,
	@processid varchar(255) = NULL
)
AS
BEGIN

DECLARE @custom1 varchar(255) = null,@custom2 varchar(255) = null,@custom3 varchar(255) = null,@custom4 varchar(255) = null,@custom5 varchar(255) = null,@iscashback bit = 0;
	DECLARE @mapsubid varchar(255) = @memberid;
	DECLARE @productid int = null,@mapsiteid varchar(255) = null;
	
--Declare standard variables
  DECLARE @user_commission decimal(18,4),@remaining_commission decimal(18,4),@user_status varchar(50),@transaction_status varchar(255);
	--Create unique record id for insert/update
  DECLARE @record_id varchar(255) = @transactionid;
  --Set the status for the transaction (pending->locked->paid)
  SET @transaction_status = LOWER(@status);

--Set the status for the user (pending->locked->payable->paid)
  SELECT @user_status = case @transaction_status
	WHEN 'paid' THEN 'payable'
	ELSE 'pending'
	END

	IF LEFT(@memberid, 4) = 'cus_' 
	BEGIN
		EXEC comm_CustomCommission @record_id=@record_id,@transaction_id=@transactionid,@network_id=@networkid,@merchant_map_id=@merchantmapid,@transaction_status=@transaction_status,@transaction_date=@transaction_date,@transaction_amount=@gross_sales,@transaction_commission=@gross_total_commission,@sub_id=@memberid
		RETURN;
	END


	BEGIN
	  EXEC comm_GetSubId @subid=@mapsubid OUT,@productid=@productid OUT,@siteid=@siteid OUT,@custom1=@custom1 OUT,@custom2=@custom2 OUT,@custom3=@custom3 OUT,@custom4=@custom4 OUT,@custom5=@custom5 OUT,@iscashback=@iscashback OUT;
	END



--Get Offer
--Get Offer
  DECLARE @commissiongross bit=1,@slmcommissiongross bit=1,@offer_multiplier decimal(18,4)=1,@offer_offertype varchar(255),@offer_slmcommission_percent decimal(18,4)=0,@offer_commission_percent decimal(18,4)=0,@slm_commission decimal(18,4)=0,@slm_expired decimal(18,4)=0
  EXEC comm_GetCommissionSLM @commission=@offer_commission_percent OUT,@offertype=@offer_offertype OUT,@multiplier=@offer_multiplier OUT,@slmcommission=@offer_slmcommission_percent OUT,@slmcommissiongross=@slmcommissiongross OUT,@commissiongross=@commissiongross OUT, @merchantmapid=@merchantmapid,@offerid=@offerid,@record_id=@record_id,@siteid=@siteid;


--End Get Offer

--/* 

  declare @multiplied_commission_amount decimal(18,4) = @total_commission * @offer_multiplier;
--Calculate the user's percent of commission the rest goes to remaining for indi
  SET @user_commission=ROUND(@multiplied_commission_amount * @offer_commission_percent, 2);

--If no user in record all goes into remaining
  IF(@memberid IS NULL OR @memberid='')
  BEGIN
	SET @user_commission = 0;
	SET @offer_commission_percent=0;
  END
 ELSE
  BEGIN
  --Single Level Marketing
	DECLARE @hostchannelid VARCHAR(255),@expirationdate DateTime,@offersiteid varchar(255);
	BEGIN
		SELECT @hostchannelid=Custom4,@expirationdate=Expiration,@offersiteid=SiteId FROM Management_LinkSubIds WHERE SubId=@memberid;
	END
	IF(@hostchannelid IS NOT NULL AND @hostchannelid !='')
	BEGIN
		IF(@offer_slmcommission_percent <= 0)
		BEGIN
		 SELECT @offer_slmcommission_percent=SLMDefaultCommissionProducts/100.00 FROM Management_SiteProperties WHERE SiteId=@offersiteid
		END
		IF(@offer_slmcommission_percent > 0)
		BEGIN
			IF(getdate()<=@expirationdate)
			BEGIN
				SET @slm_commission=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
			ELSE IF(@offer_offertype='slm')
			BEGIN
				SET @slm_expired=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
		END
	END
	--End SLM
  END

  SET @remaining_commission = (@multiplied_commission_amount-@user_commission-@slm_commission-@slm_expired);



--Update if exist else insert into the affiliates feed table
  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	IF EXISTS (SELECT 1 FROM Feed_Commissions_Rakuten WHERE record_id = @record_id)
	BEGIN
		IF EXISTS (SELECT 1 FROM Feed_Commissions_Rakuten WHERE record_id = @record_id AND commission_payout_id IS NULL)
		BEGIN
--Record is still open so update all fields except original user percent of commission
		  UPDATE Feed_Commissions_Rakuten
		  SET 
			siteid=@siteid,
			orderid=@orderid,
			transaction_date=@transaction_date,
			merchantid = @merchantid,
			advertiser_name = @advertiser_name,
			memberid = @memberid,
			sku = @sku,
			product_name = @product_name,
			sales = @sales,
			gross_sales = @gross_sales,
			total_commission = @total_commission,
			adjusted_commission = @adjusted_commission,
			baseline_commission = @baseline_commission,
			gross_commission = @gross_commission,
			gross_total_commission = @gross_total_commission,
			number_of_items = @number_of_items,
			number_of_cancelled_items = @number_of_cancelled_items,
			number_of_net_items = @number_of_net_items,
			referrer_url = @referrer_url,
			offerid = @offerid,
			[status]=@status,
			networkid=@networkid,
			merchantmapid=@merchantmapid,
			user_status=@user_status,
			transaction_status = @transaction_status,
			user_commission = ROUND(@multiplied_commission_amount * user_commission_percent,2),--on update must use original user percent from insert
			remaining_commission = (@multiplied_commission_amount - ROUND(@multiplied_commission_amount * user_commission_percent,2))--on update must use original user percent from insert
			WHERE record_id=@record_id;
		END
		ELSE
		BEGIN
--Record is closed do to payment so just update status
		UPDATE Feed_Commissions_Rakuten
		  SET 
			[status] = @status
			WHERE record_id=@record_id;
		END
	END
	ELSE
	BEGIN
--Record does not exist so insert full record
	  INSERT INTO Feed_Commissions_Rakuten(record_id,transactionid,siteid,orderid,transaction_date,merchantid,advertiser_name,memberid,sku,product_name,sales,gross_sales,total_commission,adjusted_commission,baseline_commission,gross_commission,gross_total_commission,number_of_items,number_of_cancelled_items,number_of_net_items,referrer_url,offerid,[status],networkid,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status,transaction_status)
	  VALUES
	  (@record_id,@transactionid,@siteid,@orderid,@transaction_date,@merchantid,@advertiser_name,@memberid,@sku,@product_name,@sales,@gross_sales,@total_commission,@adjusted_commission,@baseline_commission,@gross_commission,@gross_total_commission,@number_of_items,@number_of_cancelled_items,@number_of_net_items,@referrer_url,@offerid,@status,@networkid,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status,@transaction_status);
	END
	COMMIT TRANSACTION;
	BEGIN
	
--Send record to combined commissions table
		EXEC comm_SetCommission 
		    @RecordId = @record_id,
			@TransactionId = @transactionid,
			@NetworkId = @networkid,
			@MerchantMapId = @merchantmapid,
			@TransactionDate = @transaction_date,
			@TransactionStatus = @transaction_status,
			@TransactionAmount = @sales,
			@TransactionCommission = @multiplied_commission_amount,
			@UserId = @mapsubid,
			@UserStatus = @user_status,
			@UserCommissionPercent = @offer_commission_percent,
			@UserCommission = @user_commission,
			@RemainingCommission = @remaining_commission,
			@ProductId=@productid,
			@SiteId=@siteid,
			@Custom1=@custom1,
			@Custom2=@custom2,
			@Custom3=@custom3,
			@Custom4=@custom4,
			@Custom5=@custom5,
			@Quantity=@number_of_items,
			@SLMCommission=@slm_commission,
			@SLMCommissionExpired=@slm_expired,
			@SLMCommissionPercent=@offer_slmcommission_percent,
			@Sku=@sku,
			@ProductName=@product_name,
			@Comment=@advertiser_name;
	END
	BEGIN
		DECLARE @EarningsSource varchar(50)='affiliatecommissions';
		IF(@iscashback=1)
		BEGIN
			SET @EarningsSource = 'affiliatecashback';
		END
	END
	BEGIN
		EXEC comm_SetEarnings @record_id, @memberid, @EarningsSource;
	END
--Save to record history for processid
	BEGIN
		INSERT INTO Feed_History_Rakuten(record_id,processid,transactionid,siteid,orderid,transaction_date,merchantid,advertiser_name,memberid,sku,product_name,sales,gross_sales,total_commission,adjusted_commission,baseline_commission,gross_commission,gross_total_commission,number_of_items,number_of_cancelled_items,number_of_net_items,referrer_url,offerid,[status],networkid,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status,transaction_status)
		VALUES(@record_id,@processid,@transactionid,@siteid,@orderid,@transaction_date,@merchantid,@advertiser_name,@memberid,@sku,@product_name,@sales,@gross_sales,@total_commission,@adjusted_commission,@baseline_commission,@gross_commission,@gross_total_commission,@number_of_items,@number_of_cancelled_items,@number_of_net_items,@referrer_url,@offerid,@status,@networkid,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status,@transaction_status);
	END
	END
