/****** Object:  StoredProcedure [dbo].[comm_SetEarnings]    Script Date: 11/6/2020 11:27:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      BEC
-- Create Date: 10/3/2019
-- Description: Insert or Update network commissions in combined (Management_Commissions) table
-- =============================================
ALTER PROCEDURE [dbo].[comm_SetEarnings]
(
	@RecordId varchar(255),
	@LinkSubId varchar(50) = '',
	@EarningSource varchar(50) = 'affiliatecommissions'
)
AS
BEGIN


  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	--If record exist check which update to run
	IF EXISTS (SELECT 1 FROM Management_Earnings WHERE Id = @RecordId)
	BEGIN
	--If status paid only update transaction status all others are locked in do to payment
		IF EXISTS (SELECT 1 FROM Management_Earnings WHERE Id = @RecordId AND (Status = 'paid' OR SLMUserStatus = 'paid'  OR ManualOverride=1 OR LockedForPayout=1))
		BEGIN
			UPDATE Management_Earnings
			SET Management_Earnings.PaymentStatus=Management_Commissions.TransactionStatus
			FROM Management_Earnings,Management_Commissions
			WHERE Management_Earnings.Id = @RecordId
			AND Management_Earnings.Id=Management_Commissions.RecordId;
		END
		ELSE
		--Else: Not status paid, record is still open so update full record
		BEGIN
			UPDATE Management_Earnings
			SET Management_Earnings.PaymentStatus=Management_Commissions.TransactionStatus,
			Management_Earnings.OrderDate=Management_Commissions.TransactionDate,
			Management_Earnings.TotalAmount = Management_Commissions.TransactionAmount,
			Management_Earnings.LinkSubId=@LinkSubId,
			Management_Earnings.AccountId=Management_Commissions.UserId,
			Management_Earnings.MerchantMapId=Management_Commissions.MerchantMapId,
			Management_Earnings.CommissionId=Management_Commissions.RecordId,
			Management_Earnings.ProductId=Management_Commissions.ProductId,
			Management_Earnings.Status=Management_Commissions.UserStatus,
			Management_Earnings.SLMUserStatus = Management_Commissions.UserStatus,
			Management_Earnings.UserCommissionPercent=Management_Commissions.UserCommissionPercent,
			Management_Earnings.UserCommission=Management_Commissions.UserCommission,
			Management_Earnings.RemainingCommission=Management_Commissions.RemainingCommission,
			Management_Earnings.ChannelId=Management_Commissions.Custom1,
			Management_Earnings.CollectionId=Management_Commissions.Custom2,
			Management_Earnings.ContentId=Management_Commissions.Custom3,
			Management_Earnings.HostChannelId=Management_Commissions.Custom4,
			Management_Earnings.SiteId=Management_Commissions.SiteId,
			Management_Earnings.SLMCommission=Management_Commissions.SLMCommission,
			Management_Earnings.SlmCommissionPercentGross=1,
			Management_Earnings.UserCommissionPercentGross=1,
			Management_Earnings.SlmCommissionPercent=Management_Commissions.SLMCommissionPercent,
			Management_Earnings.TotalFee=0,
			Management_Earnings.ProductType='affiliatecommissions',
			Management_Earnings.EarningSource=@EarningSource,
			Management_Earnings.SLMCommissionExpired=Management_Commissions.SLMCommissionExpired,
			Management_Earnings.ManualOverride = Management_Commissions.ManualOverride,
			Management_Earnings.LockedForPayout= Management_Commissions.LockedForPayout
			FROM Management_Earnings,Management_Commissions
			WHERE Management_Earnings.Id = @RecordId
			AND Management_Earnings.Id=Management_Commissions.RecordId;
		END
	END
	ELSE
	--Else: No record so insert
	BEGIN
	  INSERT INTO Management_Earnings(Id,SiteId,AccountId,ChannelId,CollectionId,ContentId,ProductType,EarningSource,OrderDate,Currency,Status,TotalAmount,UserCommissionPercent,UserCommissionPercentGross,UserCommission,
									 SLMCommission,SlmCommissionPercent,SlmCommissionPercentGross,RemainingCommission,TotalFee,ManualOverride,PaymentStatus,HostChannelId,SLMUserStatus,LinkSubId,MerchantMapId,CommissionId,ProductId,SLMCommissionExpired,LockedForPayout)
	 SELECT RecordId,SiteId,UserId,Custom1,Custom2,Custom3,'affiliatecommissions',@EarningSource,TransactionDate,Currency,UserStatus,TransactionAmount,UserCommissionPercent,1,UserCommission,
			SLMCommission,SlmCommissionPercent,1,RemainingCommission,0,ManualOverride,TransactionStatus,Custom4,UserStatus,@LinkSubId,MerchantMapId,RecordId,ProductId,SLMCommissionExpired,LockedForPayout
	 FROM Management_Commissions WHERE RecordId=@RecordId;
	END
	
	COMMIT TRANSACTION;
	END
