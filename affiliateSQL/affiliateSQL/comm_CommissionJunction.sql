/****** Object:  StoredProcedure [dbo].[comm_CommissionJunction]    Script Date: 9/21/2021 10:28:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
ALTER PROCEDURE [dbo].[comm_CommissionJunction]
(
	@commission_id varchar(255),
	@action_status varchar(255)= NULL,
	@action_type varchar(255)= NULL,
	@aid varchar(255) = NULL,
	@country varchar(255) = NULL,
	@event_date datetime = NULL,
	@locking_date datetime = NULL,
	@order_id varchar(255) = NULL,
	@original bit = 0,
	@original_action_id varchar(255) = NULL,
	@posting_date datetime = NULL,
	@website_id varchar(255) = NULL,
	@action_tracker_id varchar(255) = NULL,
	@action_tracker_name varchar(500) = NULL,
	@cid varchar(255) = NULL,
	@advertiser_name varchar(500) = NULL,
	@commission_amount decimal(18,4) = NULL,
	@order_discount decimal(18,4) = NULL,
	@sid varchar(255) = NULL,
	@sale_amount decimal(18,4) = NULL,
	@is_cross_device bit = 0,
	@referring_url varchar(1000) = NULL,
	@browser varchar(255) = NULL,
	@device varchar(255) = NULL,
	@device_type varchar(255) NULL,
	@networkid varchar(255)= NULL,
	@merchantmapid varchar(100) = NULL,
	@processid varchar(100) = NULL,
	@offerid varchar(255) = NULL,
	@item_id varchar(255) = NULL,
	@sku varchar(255) = NULL,
	@quantity int = NULL
)
AS
BEGIN
	DECLARE @custom1 varchar(255) = null,@custom2 varchar(255) = null,@custom3 varchar(255) = null,@custom4 varchar(255) = null,@custom5 varchar(255) = null,@iscashback bit = 0;
	DECLARE @mapsubid varchar(255) = @sid;
	DECLARE @productid int = null,@siteid varchar(255) = null;
	--Declare standard variables
  DECLARE @user_commission decimal(18,4),@remaining_commission decimal(18,4),@user_status varchar(50),@transaction_status varchar(255);

	--Set the status for the transaction (pending->locked->paid)
  SET @transaction_status = LOWER(@action_status);
  --Create unique record id for insert/update
  DECLARE @record_id varchar(255) = CONCAT(@commission_id,'_',@item_id);
--Set the status for the user (pending->locked->payable->paid)
  SELECT @user_status = case @transaction_status
	WHEN 'locked' THEN 'locked'
	WHEN 'closed' THEN 'payable'
	ELSE 'pending'
	END

	IF LEFT(@sid, 4) = 'cus_' 
	BEGIN
		EXEC comm_CustomCommission @record_id=@record_id,@transaction_id=@commission_id,@network_id=@networkid,@merchant_map_id=@merchantmapid,@transaction_status=@transaction_status,@transaction_date=@event_date,@transaction_amount=@sale_amount,@transaction_commission=@commission_amount,@sub_id=@sid
		RETURN;
	END

	BEGIN
	  EXEC comm_GetSubId @subid=@mapsubid OUT,@productid=@productid OUT,@siteid=@siteid OUT,@custom1=@custom1 OUT,@custom2=@custom2 OUT,@custom3=@custom3 OUT,@custom4=@custom4 OUT,@custom5=@custom5 OUT,@iscashback=@iscashback OUT;
	END
	



--Get Offer
  DECLARE @commissiongross bit=1,@slmcommissiongross bit=1,@offer_multiplier decimal(18,4)=1,@offer_offertype varchar(255),@offer_slmcommission_percent decimal(18,4)=0,@offer_commission_percent decimal(18,4)=0,@slm_commission decimal(18,4)=0,@slm_expired decimal(18,4)=0
  EXEC comm_GetCommissionSLM @commission=@offer_commission_percent OUT,@offertype=@offer_offertype OUT,@multiplier=@offer_multiplier OUT,@slmcommission=@offer_slmcommission_percent OUT,@slmcommissiongross=@slmcommissiongross OUT,@commissiongross=@commissiongross OUT, @merchantmapid=@merchantmapid,@offerid=@offerid,@record_id=@record_id,@siteid=@siteid;

--End Get Offer

 declare @multiplied_commission_amount decimal(18,4) = @commission_amount * @offer_multiplier;

--Calculate the user's percent of commission the rest goes to remaining for indi
  SET @user_commission=ROUND(@multiplied_commission_amount * @offer_commission_percent, 2);

--If no user in record all goes into remaining
  IF(@sid IS NULL OR @sid='')
  BEGIN
	SET @user_commission = 0;
	SET @offer_commission_percent=0;
  END
   ELSE
  BEGIN
  --Single Level Marketing
	DECLARE @hostchannelid VARCHAR(255),@expirationdate DateTime,@offersiteid varchar(255);
	BEGIN
		SELECT @hostchannelid=Custom4,@expirationdate=Expiration,@offersiteid=SiteId FROM Management_LinkSubIds WHERE SubId=@sid;
	END
	IF(@hostchannelid IS NOT NULL AND @hostchannelid !='')
	BEGIN
		IF(@offer_slmcommission_percent <= 0)
		BEGIN
		 SELECT @offer_slmcommission_percent=SLMDefaultCommissionProducts/100.00 FROM Management_SiteProperties WHERE SiteId=@offersiteid
		END
		IF(@offer_slmcommission_percent > 0)
		BEGIN
			IF(getdate()<=@expirationdate)
			BEGIN
				SET @slm_commission=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
			ELSE IF(@offer_offertype='slm')
			BEGIN
				SET @slm_expired=ROUND(@multiplied_commission_amount * @offer_slmcommission_percent, 2);
			END
		END
	END
	--End SLM
  END
 SET @remaining_commission = (@multiplied_commission_amount-@user_commission-@slm_commission-@slm_expired);


	--Update if exist else insert into the affiliates feed table
  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRANSACTION;
	IF EXISTS (SELECT 1 FROM Feed_Commissions_CJ WHERE record_id = @record_id)
	BEGIN
		IF EXISTS (SELECT 1 FROM Feed_Commissions_CJ WHERE record_id = @record_id AND commission_payout_id IS NULL)
		BEGIN
--Record is still open so update all fields except original user percent of commission
		  UPDATE Feed_Commissions_CJ
		  SET 
			action_status = @action_status,
			action_type = @action_type,
			aid = @aid,
			country = @country,
			event_date = @event_date,
			locking_date = @locking_date,
			order_id = @order_id,
			original = @original,
			original_action_id = @original_action_id,
			posting_date = @posting_date,
			website_id = @website_id,
			action_tracker_id = @action_tracker_id,
			action_tracker_name = @action_tracker_name,
			cid = @cid,
			advertiser_name = @advertiser_name,
			commission_amount = @commission_amount,
			order_discount = @order_discount,
			[sid] = @sid,
			sale_amount = @sale_amount,
			is_cross_device = @is_cross_device,
			referring_url = @referring_url,
			browser = @browser,
			device= @device,
			device_type = @device_type,
			networkid=@networkid,
			merchantmapid=@merchantmapid,
			user_status=@user_status,
			transaction_status = @transaction_status,
			quantity = @quantity,
			sku = @sku,
			item_id = @item_id,
			user_commission = ROUND(@multiplied_commission_amount * user_commission_percent,2),--on update must use original user percent from insert
			remaining_commission = (@multiplied_commission_amount - ROUND(@multiplied_commission_amount * user_commission_percent,2))--on update must use original user percent from insert
			WHERE record_id=@record_id;
		END
		ELSE
		BEGIN
--Record is closed do to payment so just update status
		UPDATE Feed_Commissions_CJ
		  SET 
			action_status = @action_status
			WHERE record_id=@record_id;
		END
	END
	ELSE
	BEGIN
--Record does not exist so insert full record
	  INSERT INTO Feed_Commissions_CJ(record_id,commission_id,action_status,action_type,aid,country,event_date,locking_date,order_id,original,original_action_id,posting_date,website_id,action_tracker_id,action_tracker_name,cid,advertiser_name,commission_amount,order_discount,[sid],sale_amount,is_cross_device,referring_url,browser,device,device_type,item_id,sku,quantity,networkid,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status,transaction_status)
	  VALUES
	  (@record_id,@commission_id,@action_status,@action_type,@aid,@country,@event_date,@locking_date,@order_id,@original,@original_action_id,@posting_date,@website_id,@action_tracker_id,@action_tracker_name,@cid,@advertiser_name,@commission_amount,@order_discount,@sid,@sale_amount,@is_cross_device,@referring_url,@browser,@device,@device_type,@item_id,@sku,@quantity,@networkid,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status,@transaction_status);
	END
	COMMIT TRANSACTION;

	BEGIN
	

--Send record to combined commissions table
		EXEC comm_SetCommission 
		    @RecordId = @record_id,
			@TransactionId = @commission_id,
			@NetworkId = @networkid,
			@MerchantMapId = @merchantmapid,
			@TransactionDate = @event_date,
			@TransactionStatus = @transaction_status,
			@TransactionAmount = @sale_amount,
			@TransactionCommission = @multiplied_commission_amount,
			@UserId = @mapsubid,
			@UserStatus = @user_status,
			@UserCommissionPercent = @offer_commission_percent, 
			@UserCommission = @user_commission,
			@RemainingCommission = @remaining_commission,
			@ProductId=@productid,
			@SiteId=@siteid,
			@Custom1=@custom1,
			@Custom2=@custom2,
			@Custom3=@custom3,
			@Custom4=@custom4,
			@Custom5=@custom5,
			@Quantity=@quantity,
			@SLMCommission=@slm_commission,
			@SLMCommissionExpired=@slm_expired,
			@SLMCommissionPercent=@offer_slmcommission_percent,
			@ProductName='',
			@Comment=@advertiser_name;
	END
	BEGIN
		DECLARE @EarningsSource varchar(50)='affiliatecommissions';
		IF(@iscashback=1)
		BEGIN
			SET @EarningsSource = 'affiliatecashback';
		END
	END
	BEGIN
	    
		EXEC comm_SetEarnings @record_id, @sid, @EarningsSource;
	END
--Save to record history for processid
	BEGIN
		INSERT INTO Feed_History_CJ(record_id,processid,commission_id,action_status,action_type,aid,country,event_date,locking_date,order_id,original,original_action_id,posting_date,website_id,action_tracker_id,action_tracker_name,cid,advertiser_name,commission_amount,order_discount,[sid],sale_amount,is_cross_device,referring_url,browser,device,device_type,item_id,sku,quantity,networkid,merchantmapid,user_commission_percent,user_commission,remaining_commission,user_status,transaction_status)
		VALUES(@record_id,@processid,@commission_id,@action_status,@action_type,@aid,@country,@event_date,@locking_date,@order_id,@original,@original_action_id,@posting_date,@website_id,@action_tracker_id,@action_tracker_name,@cid,@advertiser_name,@commission_amount,@order_discount,@sid,@sale_amount,@is_cross_device,@referring_url,@browser,@device,@device_type,@item_id,@sku,@quantity,@networkid,@merchantmapid,@offer_commission_percent,@user_commission,@remaining_commission,@user_status,@transaction_status);
	END
 END
